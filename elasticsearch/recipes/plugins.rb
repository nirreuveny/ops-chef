[Chef::Recipe, Chef::Resource].each { |l| l.send :include, ::Extensions }

node.set['elasticsearch']['skip_restart'] = true

node[:elasticsearch][:plugins].each do | name, config |
#  next if name == 'elasticsearch/elasticsearch-cloud-aws' && !node.recipe?('aws')
  next if name == 'elasticsearch/elasticsearch-cloud-gce' && !node.recipe?('gce')
  install_plugin name, config
end
