[Chef::Recipe, Chef::Resource].each { |l| l.send :include, ::Extensions }

Erubis::Context.send(:include, Extensions::Templates)

service "elasticsearch" do
  supports :status => true, :restart => true
  action [ :enable ]
end

node.set['elasticsearch']['skip_restart'] = true
node.set['elasticsearch']['discovery']['zen']['ping']['unicast']['hosts'] = node['opsworks']['layers']["master"]["instances"].select{|k,v| v["status"] == "online" }.map{|k,v| v["private_ip"]}.join(',') unless node['opsworks']['layers']["master"].nil?
# Create ES config file
#
template "elasticsearch.yml" do
  path   "#{node.elasticsearch[:path][:conf]}/elasticsearch.yml"
  source node.elasticsearch[:templates][:elasticsearch_yml]
  owner  node.elasticsearch[:user] and group node.elasticsearch[:user] and mode 0755

  notifies :restart, 'service[elasticsearch]' unless node.elasticsearch[:skip_restart]
end
