[Chef::Recipe, Chef::Resource].each { |l| l.send :include, ::Extensions }

Erubis::Context.send(:include, Extensions::Templates)

filename = node.elasticsearch[:deb_url].split('/').last

remote_file "#{Chef::Config[:file_cache_path]}/#{filename}" do
  source   node.elasticsearch[:deb_url]
  checksum node.elasticsearch[:deb_sha]
  mode 00644
end

dpkg_package "#{Chef::Config[:file_cache_path]}/#{filename}" do
  action :install
end

ruby_block "Set heap size in /etc/default/elasticsearch" do
  block do
    fe = Chef::Util::FileEdit.new("/etc/default/elasticsearch")
    fe.insert_line_if_no_match(/ES_HEAP_SIZE=/, "ES_HEAP_SIZE=#{node.elasticsearch[:allocated_memory]}")
    fe.search_file_replace_line(/ES_HEAP_SIZE=/, "ES_HEAP_SIZE=#{node.elasticsearch[:allocated_memory]}") # if the value has changed but the line exists in the file
    fe.insert_line_if_no_match(/MAX_OPEN_FILES=/, "MAX_OPEN_FILES=65535")
    fe.search_file_replace_line(/MAX_OPEN_FILES=/, "MAX_OPEN_FILES=65535")
    fe.insert_line_if_no_match(/MAX_LOCKED_MEMORY=/, "MAX_LOCKED_MEMORY=unlimited")
    fe.search_file_replace_line(/MAX_LOCKED_MEMORY=/, "MAX_LOCKED_MEMORY=unlimited")
    fe.write_file
  end
end

# Create data path directories
#
data_paths = node.elasticsearch[:path][:data].is_a?(Array) ? node.elasticsearch[:path][:data] : node.elasticsearch[:path][:data].split(',')

data_paths.each do |path|
  directory path.strip do
    owner node.elasticsearch[:user] and group node.elasticsearch[:user] and mode 0755
    recursive true
    action :create
  end
end

service "elasticsearch" do
  supports :status => true, :restart => true
  action [ :enable ]
end

node.set['elasticsearch']['discovery']['zen']['ping']['unicast']['hosts'] = node['opsworks']['layers']["master"]["instances"].select{|k,v| v["status"] == "online" }.map{|k,v| v["private_ip"]}.join(',') unless node['opsworks']['layers']["master"].nil?
# Create ES config file
#
template "elasticsearch.yml" do
  path   "#{node.elasticsearch[:path][:conf]}/elasticsearch.yml"
  source node.elasticsearch[:templates][:elasticsearch_yml]
  owner  node.elasticsearch[:user] and group node.elasticsearch[:user] and mode 0755

  notifies :restart, 'service[elasticsearch]' unless node.elasticsearch[:skip_restart]
end
